-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 09-07-2015 a las 23:37:08
-- Versión del servidor: 5.6.24-0ubuntu2
-- Versión de PHP: 5.6.4-4ubuntu6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ecommerce`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_classes`
--

CREATE TABLE IF NOT EXISTS `acl_classes` (
`id` int(10) unsigned NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `acl_classes`
--

INSERT INTO `acl_classes` (`id`, `class_type`) VALUES
(1, 'Fhios\\EcommerceBundle\\Entity\\Product');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_entries`
--

CREATE TABLE IF NOT EXISTS `acl_entries` (
`id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `acl_entries`
--

INSERT INTO `acl_entries` (`id`, `class_id`, `object_identity_id`, `security_identity_id`, `field_name`, `ace_order`, `mask`, `granting`, `granting_strategy`, `audit_success`, `audit_failure`) VALUES
(1, 1, 1, 1, NULL, 0, 12, 1, 'all', 0, 0),
(2, 1, 2, 1, NULL, 0, 12, 1, 'all', 0, 0),
(3, 1, 3, 1, NULL, 0, 12, 1, 'all', 0, 0),
(4, 1, 4, 1, NULL, 0, 12, 1, 'all', 0, 0),
(5, 1, 5, 1, NULL, 0, 12, 1, 'all', 0, 0),
(6, 1, 6, 1, NULL, 0, 12, 1, 'all', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_object_identities`
--

CREATE TABLE IF NOT EXISTS `acl_object_identities` (
`id` int(10) unsigned NOT NULL,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `acl_object_identities`
--

INSERT INTO `acl_object_identities` (`id`, `parent_object_identity_id`, `class_id`, `object_identifier`, `entries_inheriting`) VALUES
(1, NULL, 1, '1', 1),
(2, NULL, 1, '2', 1),
(3, NULL, 1, '3', 1),
(4, NULL, 1, '4', 1),
(5, NULL, 1, '5', 1),
(6, NULL, 1, '6', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_object_identity_ancestors`
--

CREATE TABLE IF NOT EXISTS `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `acl_object_identity_ancestors`
--

INSERT INTO `acl_object_identity_ancestors` (`object_identity_id`, `ancestor_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_security_identities`
--

CREATE TABLE IF NOT EXISTS `acl_security_identities` (
`id` int(10) unsigned NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `acl_security_identities`
--

INSERT INTO `acl_security_identities` (`id`, `identifier`, `username`) VALUES
(1, 'ROLE_ADMIN', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `E_BuyOrder`
--

CREATE TABLE IF NOT EXISTS `E_BuyOrder` (
`id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `customer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customerId` int(11) NOT NULL,
  `customerEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shippingAddress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billingAddress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `totalAmount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `E_BuyOrder`
--

INSERT INTO `E_BuyOrder` (`id`, `created`, `customer`, `customerId`, `customerEmail`, `shippingAddress`, `billingAddress`, `totalAmount`, `status`) VALUES
(1, '2015-07-09 23:29:16', 'Jordi  Gracia Quero', 1, 'jordi.gracia@fhios.es', 'Shipping Address', 'Billing Address', '5560', 'PENDING');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `E_cart`
--

CREATE TABLE IF NOT EXISTS `E_cart` (
`id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `buy_order_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `E_cart`
--

INSERT INTO `E_cart` (`id`, `customer_id`, `buy_order_id`, `created`) VALUES
(1, 1, NULL, '2015-07-09 19:29:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `E_cart_item`
--

CREATE TABLE IF NOT EXISTS `E_cart_item` (
`id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `E_cart_item`
--

INSERT INTO `E_cart_item` (`id`, `product_id`, `cart_id`, `price`, `quantity`) VALUES
(1, 1, 1, 1450, 1),
(2, 4, 1, 459, 2),
(3, 2, 1, 699, 1),
(4, 3, 1, 799, 1),
(5, 3, 1, 799, 1),
(6, 5, 1, 49, 2),
(7, 2, 1, 699, 1),
(8, 5, 1, 49, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `E_category`
--

CREATE TABLE IF NOT EXISTS `E_category` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `E_category`
--

INSERT INTO `E_category` (`id`, `name`, `description`) VALUES
(1, 'Portátiles', 'Portátiles'),
(2, 'Smartphones', 'smartphones'),
(3, 'Wearables', 'Wearables'),
(4, 'Videojuegos / Consolas', 'Videojuegos / Consolas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `E_product`
--

CREATE TABLE IF NOT EXISTS `E_product` (
`id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `product` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `E_product`
--

INSERT INTO `E_product` (`id`, `category_id`, `name`, `price`, `description`, `active`, `product`, `updated_at`) VALUES
(1, 1, 'MacBook Pro 13''''', 1450.00, 'Apple MacBook Pro 13''''', 1, 'apple_15inch_macbook_pro_with_retina_display_1218250_g1.jpg', '2015-07-09 23:29:16'),
(2, 2, 'iPhone 6', 699.00, 'Apple Iphone 6 16Gb', 1, 'iphone6-select-2014.jpeg', '2015-07-09 23:29:16'),
(3, 2, 'iPhone 6 Plus', 799.00, 'Apple iPhone 6 Plus 16Gb', 1, 'iphone6-select-2014.jpeg', '2015-07-09 23:29:16'),
(4, 3, 'Apple Watch', 459.00, 'New Apple watch', 1, 'apple-watch-logo-040215.png', '2015-07-09 23:29:16'),
(5, 4, 'The Last Of Us Remastered', 49.00, 'The Last Of Us Remastered version PS4', 1, '111780-L-LO.jpg', '2015-07-09 23:29:16'),
(6, 4, 'The Witcher', 59.90, 'The Witcher PS4', 1, 'the_witcher_3-2546818.jpg', '2015-07-09 22:03:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `E_tag`
--

CREATE TABLE IF NOT EXISTS `E_tag` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_review`
--

CREATE TABLE IF NOT EXISTS `product_review` (
`id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `product_review`
--

INSERT INTO `product_review` (`id`, `product_id`, `description`, `created`) VALUES
(1, 2, 'Best Phone Ever', '2015-07-01 00:00:00'),
(2, 5, 'Best GAME ever', '2015-07-09 22:43:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `U_Address`
--

CREATE TABLE IF NOT EXISTS `U_Address` (
`id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `U_Address`
--

INSERT INTO `U_Address` (`id`, `customer_id`, `street`, `number`, `city`, `type`) VALUES
(1, 1, 'Av Via Augusta', '132', 'Sant Cugat del Valles', 'Billing');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `U_fos_user`
--

CREATE TABLE IF NOT EXISTS `U_fos_user` (
`id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middlenames` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `U_fos_user`
--

INSERT INTO `U_fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `name`, `middlenames`) VALUES
(1, 'jgracia', 'jgracia', 'jordi.gracia@fhios.es', 'jordi.gracia@fhios.es', 1, 'cj6vvsszr6gc8os8gs0ko80kssss40c', 'Wi9XEPdd+yMk75f1p5CPL3kV675bQD7Gk05DUYvZM8OPUeNXJSy7mj88fDxpmTafIHeNUN+jEnsaIZVohd5baw==', '2015-07-09 19:29:44', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL, 'Jordi', 'Gracia Quero');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acl_classes`
--
ALTER TABLE `acl_classes`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Indices de la tabla `acl_entries`
--
ALTER TABLE `acl_entries`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`), ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`), ADD KEY `IDX_46C8B806EA000B10` (`class_id`), ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`), ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Indices de la tabla `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`), ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Indices de la tabla `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
 ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`), ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`), ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Indices de la tabla `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Indices de la tabla `E_BuyOrder`
--
ALTER TABLE `E_BuyOrder`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `E_cart`
--
ALTER TABLE `E_cart`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_7EF335CD7FC358ED` (`buy_order_id`), ADD KEY `IDX_7EF335CD9395C3F3` (`customer_id`);

--
-- Indices de la tabla `E_cart_item`
--
ALTER TABLE `E_cart_item`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_9270E80D4584665A` (`product_id`), ADD KEY `IDX_9270E80D1AD5CDBF` (`cart_id`);

--
-- Indices de la tabla `E_category`
--
ALTER TABLE `E_category`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `E_product`
--
ALTER TABLE `E_product`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_B02C685F12469DE2` (`category_id`);

--
-- Indices de la tabla `E_tag`
--
ALTER TABLE `E_tag`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `product_review`
--
ALTER TABLE `product_review`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_1B3FC0624584665A` (`product_id`);

--
-- Indices de la tabla `U_Address`
--
ALTER TABLE `U_Address`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_D55789E9395C3F3` (`customer_id`);

--
-- Indices de la tabla `U_fos_user`
--
ALTER TABLE `U_fos_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_E10410CD92FC23A8` (`username_canonical`), ADD UNIQUE KEY `UNIQ_E10410CDA0D96FBF` (`email_canonical`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acl_classes`
--
ALTER TABLE `acl_classes`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `acl_entries`
--
ALTER TABLE `acl_entries`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `E_BuyOrder`
--
ALTER TABLE `E_BuyOrder`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `E_cart`
--
ALTER TABLE `E_cart`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `E_cart_item`
--
ALTER TABLE `E_cart_item`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `E_category`
--
ALTER TABLE `E_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `E_product`
--
ALTER TABLE `E_product`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `E_tag`
--
ALTER TABLE `E_tag`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `product_review`
--
ALTER TABLE `product_review`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `U_Address`
--
ALTER TABLE `U_Address`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `U_fos_user`
--
ALTER TABLE `U_fos_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acl_entries`
--
ALTER TABLE `acl_entries`
ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Filtros para la tabla `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `E_cart`
--
ALTER TABLE `E_cart`
ADD CONSTRAINT `FK_7EF335CD7FC358ED` FOREIGN KEY (`buy_order_id`) REFERENCES `E_BuyOrder` (`id`),
ADD CONSTRAINT `FK_7EF335CD9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `U_fos_user` (`id`);

--
-- Filtros para la tabla `E_cart_item`
--
ALTER TABLE `E_cart_item`
ADD CONSTRAINT `FK_9270E80D1AD5CDBF` FOREIGN KEY (`cart_id`) REFERENCES `E_cart` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_9270E80D4584665A` FOREIGN KEY (`product_id`) REFERENCES `E_product` (`id`);

--
-- Filtros para la tabla `E_product`
--
ALTER TABLE `E_product`
ADD CONSTRAINT `FK_B02C685F12469DE2` FOREIGN KEY (`category_id`) REFERENCES `E_category` (`id`);

--
-- Filtros para la tabla `product_review`
--
ALTER TABLE `product_review`
ADD CONSTRAINT `FK_1B3FC0624584665A` FOREIGN KEY (`product_id`) REFERENCES `E_product` (`id`);

--
-- Filtros para la tabla `U_Address`
--
ALTER TABLE `U_Address`
ADD CONSTRAINT `FK_D55789E9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `U_fos_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
