<?php
// src/Acme/UserBundle/Entity/User.php

namespace Fhios\UserBundle\Entity;

//require_once "../vendor/friendsofsymfony/user-bundle/"
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="U_fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="middlenames", type="string", length=255)
     */
    private $middlenames;
    
    /**
     * @ORM\OneToMany(targetEntity="Fhios\EcommerceBundle\Entity\Cart", mappedBy="customer")
     */
    protected $cart;

    /**
     * @ORM\OneToMany(targetEntity="Fhios\UserBundle\Entity\Address", mappedBy="customer")
     */
    protected $address;
    
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add cart
     *
     * @param \Fhios\EcommerceBundle\Entity\Cart $cart
     * @return User
     */
    public function addCart(\Fhios\EcommerceBundle\Entity\Cart $cart)
    {
        $this->cart[] = $cart;

        return $this;
    }

    /**
     * Remove cart
     *
     * @param \Fhios\EcommerceBundle\Entity\Cart $cart
     */
    public function removeCart(\Fhios\EcommerceBundle\Entity\Cart $cart)
    {
        $this->cart->removeElement($cart);
    }

    /**
     * Get cart
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Add address
     *
     * @param \Fhios\UserBundle\Entity\Address $address
     * @return User
     */
    public function addAddress(\Fhios\UserBundle\Entity\Address $address)
    {
        $this->address[] = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param \Fhios\UserBundle\Entity\Address $address
     */
    public function removeAddress(\Fhios\UserBundle\Entity\Address $address)
    {
        $this->address->removeElement($address);
    }

    /**
     * Get address
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set middlenames
     *
     * @param string $middlenames
     * @return User
     */
    public function setMiddlenames($middlenames)
    {
        $this->middlenames = $middlenames;

        return $this;
    }

    /**
     * Get middlenames
     *
     * @return string 
     */
    public function getMiddlenames()
    {
        return $this->middlenames;
    }
}
