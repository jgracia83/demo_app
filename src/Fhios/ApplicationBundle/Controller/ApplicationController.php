<?php

namespace Fhios\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ApplicationController extends Controller
{
    
    /*
    * RENDERIZA HOMEPAGE
    */
    public function homeAction($category, $page)
    {
        $em = $this->getDoctrine()->getManager();        
        $pageSize = 6;

        if ($category > 0 )
        {
            $paginator = $em->getRepository('EcommerceBundle:Product')
                        ->findAllOrderedByNameAndCategory($category, $pageSize, $page);   
        }else{
        
            $paginator = $em->getRepository('EcommerceBundle:Product')
                        ->findAllOrderedByName($pageSize, $page);
        }
        
        $totalItems = count($paginator);
        $pagesCount = ceil($totalItems / $pageSize);

        return $this->render('ApplicationBundle:Home:homepage.html.twig', 
        	array(
                'products'      =>  $paginator,
                'category'      =>  $category,
                'totalItems'    =>  $totalItems,
                'pagesCount'    =>  $pagesCount,
        		));
    }

    
    /*
    * RENDERIZA CART EN BARRAPRINCIPAL
    */
    public function cartAction()
    {
        $em = $this->getDoctrine()->getManager();        
        $user = $this->getUser();
        $authorizationChecker = $this->get('security.authorization_checker');

        if ($authorizationChecker->isGranted('ROLE_USER'))
        {
            $cart = $em->getRepository('EcommerceBundle:Cart')->findOneByCustomer($user);

        }

        if($cart){           
            return $this->render('ApplicationBundle:Templates:cart.html.twig', 
                array(
                    'cart'  => $cart,
                    ));
        }
        return $this->render('ApplicationBundle:Templates:cart.html.twig', 
                array(
                    //'cart'  => $cart,
                    ));
    }

    
    public function faqAction()
    {
    	return $this->render('ApplicationBundle:Faq:faq.html.twig', array());	
    }

    public function newsletterAction()
    {
        $from = "jordi.gracia.fhios@gmail.com";
        $to = "jordi.gracia.fhios@gmail.com";
        $body = "This is email body";
        $subject = "This is email subject";
        $this->get('ecommerce.mailer')->sendEmail($from, $to, $subject, $body);
    }

}
