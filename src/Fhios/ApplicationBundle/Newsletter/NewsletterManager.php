<?php
// src/Acme/HelloBundle/Newsletter/NewsletterManager.php
namespace Fhios\ApplicationBundle\Newsletter;

use Fhios\ApplicationBundle\Mailer\Mailer;

class NewsletterManager
{
    protected $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    // ...
}
