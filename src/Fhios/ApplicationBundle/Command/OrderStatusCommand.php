<?php
namespace Fhios\ApplicationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class OrderStatusCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fhios:buyOrder:status')
            ->setDescription('Buy Order status')
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                'Who do you want to greet?'
            )
            ->addOption(
                'yell',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will yell in uppercase letters'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        if ($name) {
            $text = 'Hello '.$name;
        } else {
            $text = 'Hello';
        }

        if ($input->getOption('yell')) {
            $text = strtoupper($text);
        }

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /* LOGS DE SYMFONY*/
        $logger = $this->getContainer()->get('logger');
        $logger->info('Executing command for '.$name);

        $output->writeln($text);
    }
}