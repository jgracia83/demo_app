<?php

namespace Fhios\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BackendController extends Controller
{
    public function backendAction()
    {
        return $this->render('BackendBundle:Backend:backend.html.twig', array());
    }
}
