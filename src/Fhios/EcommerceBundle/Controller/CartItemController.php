<?php

namespace Fhios\EcommerceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fhios\EcommerceBundle\Entity\Cart;
use Fhios\EcommerceBundle\Entity\CartItem;
use Fhios\EcommerceBundle\Form\Type\CartItemType;
use Fhios\EcommerceBundle\Form\Type\AddToCartType;

/**
 * CartItem controller.
 *
 */
class CartItemController extends Controller
{

    /**
     * Lists all CartItem entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EcommerceBundle:CartItem')->findAll();

        return $this->render('EcommerceBundle:CartItem:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new CartItem entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new CartItem();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cartitem_show', array('id' => $entity->getId())));
        }

        return $this->render('EcommerceBundle:CartItem:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a CartItem entity.
     *
     * @param CartItem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CartItem $entity)
    {
        $form = $this->createForm(new CartItemType(), $entity, array(
            'action' => $this->generateUrl('cartitem_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CartItem entity.
     *
     */
    public function newAction()
    {
        $entity = new CartItem();
        $form   = $this->createCreateForm($entity);

        return $this->render('EcommerceBundle:CartItem:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CartItem entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EcommerceBundle:CartItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CartItem entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EcommerceBundle:CartItem:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CartItem entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EcommerceBundle:CartItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CartItem entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EcommerceBundle:CartItem:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CartItem entity.
    *
    * @param CartItem $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CartItem $entity)
    {
        $form = $this->createForm(new CartItemType(), $entity, array(
            'action' => $this->generateUrl('cartitem_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CartItem entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EcommerceBundle:CartItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CartItem entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cartitem_edit', array('id' => $id)));
        }

        return $this->render('EcommerceBundle:CartItem:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a CartItem entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EcommerceBundle:CartItem')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CartItem entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cartitem'));
    }

    /**
     * Creates a form to delete a CartItem entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cartitem_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    
    /**
     * Creates a form to create a CartItem entity.
     *
     * @param CartItem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createAddToCartForm(CartItem $entity, $id)
    {
        $form = $this->createForm(new AddToCartType(), $entity, array(
            'action' => $this->generateUrl('cartitem_add_create', array('productId'=>$id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Add to Cart', 'attr'=> array('class'=>'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new CartItem entity.
     *
     */
    public function addToCartAction($productId)
    {
        
        $entity = new CartItem();
        $form   = $this->createAddToCartForm($entity, $productId);

        return $this->render('EcommerceBundle:CartItem:addToCart.html.twig', array(
            'product'=> $productId,
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new CartItem entity.
     *
     */
    public function createCartItemAction(Request $request, $productId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        
        $entity = new CartItem();
        
        $cart = $em->getRepository('EcommerceBundle:Cart')->findOneByCustomer($user);

        if (!$cart){
            $cart = new Cart();
            $cart->setCreated(new \DateTime);
            $cart->setCustomer($user);
            $em->persist($cart);
        }
            
        $form = $this->createAddToCartForm($entity, $productId);
        $form->handleRequest($request);

        $product = $em->getRepository('EcommerceBundle:Product')->findOneById($productId);
        if ($form->isValid()) {

            $entity->setPrice($product->getPrice());
            $entity->setProduct($product);
            $entity->setCart($cart);
            $em->persist($entity);
            
            $em->flush();
            $this->get('session')->getFlashBag()->add('info', 'Producto añadido al carrito');
        
            return $this->redirect($this->generateUrl('application_homepage'));
            //return $this->redirect($this->generateUrl('cartitem_show', array('id' => $entity->getId())));
        }

        return $this->render('EcommerceBundle:CartItem:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
}
