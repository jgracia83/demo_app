<?php

namespace Fhios\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('EcommerceBundle:Product')
        				->findAllOrderedByName();

        return $this->render('EcommerceBundle:Default:index.html.twig', 
        	array(
        		'products' => $products
        		));
    }

    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    	$product = $em->getRepository('EcommerceBundle:Product')
                    	->findOneById($id);
        
        return $this->render('EcommerceBundle:Default:show.html.twig', 
        	array(
        		'product' => $product
        		));
    }

    public function updateAction($id)
    {

    }

    public function removeAction($id)
    {

    }

}
