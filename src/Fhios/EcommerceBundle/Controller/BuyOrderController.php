<?php

namespace Fhios\EcommerceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Fhios\EcommerceBundle\Entity\BuyOrder;
use Fhios\EcommerceBundle\Form\BuyOrderType;

/**
 * BuyOrder controller.
 *
 */
class BuyOrderController extends Controller
{

    public function completeAction($cartId)
    {
        $em = $this->getDoctrine()->getManager();

        $cart = $em->getRepository('EcommerceBundle:Cart')->findOneById($cartId);
                $total = $this->get('ecommerce.utils')->totalCart($cartId);

        $user = $this->getUser();

        $buyOrder = new BuyOrder();
        $buyOrder
            ->setCreated(new \Datetime())
            ->setCustomer($user->getName()."  ".$user->getMiddlenames())
            ->setCustomerId($user->getId())
            ->setCustomerEmail($user->getEmail())
            ->setShippingAddress("Shipping Address")
            ->setBillingAddress("Billing Address")
            ->setTotalAmount($total)
            ->setStatus("PENDING")
            ->setCart($cart)
            ;
        $em->persist($buyOrder);
        $em->flush();

        $this->get('session')->getFlashBag()->add('info', 'Orden de compra realizada');
        
        return $this->redirect($this->generateUrl('application_homepage'));
    }

    /**
     * Lists all BuyOrder entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EcommerceBundle:BuyOrder')->findAll();

        return $this->render('EcommerceBundle:BuyOrder:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new BuyOrder entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new BuyOrder();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('buyorder_show', array('id' => $entity->getId())));
        }

        return $this->render('EcommerceBundle:BuyOrder:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a BuyOrder entity.
     *
     * @param BuyOrder $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(BuyOrder $entity)
    {
        $form = $this->createForm(new BuyOrderType(), $entity, array(
            'action' => $this->generateUrl('buyorder_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new BuyOrder entity.
     *
     */
    public function newAction()
    {
        $entity = new BuyOrder();
        $form   = $this->createCreateForm($entity);

        return $this->render('EcommerceBundle:BuyOrder:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a BuyOrder entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EcommerceBundle:BuyOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BuyOrder entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EcommerceBundle:BuyOrder:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing BuyOrder entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EcommerceBundle:BuyOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BuyOrder entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EcommerceBundle:BuyOrder:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a BuyOrder entity.
    *
    * @param BuyOrder $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(BuyOrder $entity)
    {
        $form = $this->createForm(new BuyOrderType(), $entity, array(
            'action' => $this->generateUrl('buyorder_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing BuyOrder entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EcommerceBundle:BuyOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BuyOrder entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('buyorder_edit', array('id' => $id)));
        }

        return $this->render('EcommerceBundle:BuyOrder:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a BuyOrder entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EcommerceBundle:BuyOrder')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BuyOrder entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('buyorder'));
    }

    /**
     * Creates a form to delete a BuyOrder entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('buyorder_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
