<?php

namespace Fhios\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cart
 *
 * @ORM\Table(name="E_cart")
 * @ORM\Entity(repositoryClass="Fhios\EcommerceBundle\Entity\CartRepository")
 */
class Cart
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="Fhios\UserBundle\Entity\User", inversedBy="cart")
     */
    protected $customer;

    /**
    * @ORM\OneToMany(targetEntity="CartItem", mappedBy="cart")
    */
    protected $cartItem;

    /**
    * @ORM\OneToOne(targetEntity="BuyOrder", inversedBy="cart")
    */
    protected $buyOrder;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Cart
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartItem = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set customer
     *
     * @param \Fhios\UserBundle\Entity\User $customer
     * @return Cart
     */
    public function setCustomer(\Fhios\UserBundle\Entity\User $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Fhios\UserBundle\Entity\User 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Add cartItem
     *
     * @param \Fhios\EcommerceBundle\Entity\CartItem $cartItem
     * @return Cart
     */
    public function addCartItem(\Fhios\EcommerceBundle\Entity\CartItem $cartItem)
    {
        $this->cartItem[] = $cartItem;

        return $this;
    }

    /**
     * Remove cartItem
     *
     * @param \Fhios\EcommerceBundle\Entity\CartItem $cartItem
     */
    public function removeCartItem(\Fhios\EcommerceBundle\Entity\CartItem $cartItem)
    {
        $this->cartItem->removeElement($cartItem);
    }

    /**
     * Get cartItem
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCartItem()
    {
        return $this->cartItem;
    }

    /**
     * Set buyOrder
     *
     * @param \Fhios\EcommerceBundle\Entity\BuyOrder $buyOrder
     * @return Cart
     */
    public function setBuyOrder(\Fhios\EcommerceBundle\Entity\BuyOrder $buyOrder = null)
    {
        $this->buyOrder = $buyOrder;

        return $this;
    }

    /**
     * Get buyOrder
     *
     * @return \Fhios\EcommerceBundle\Entity\BuyOrder 
     */
    public function getBuyOrder()
    {
        return $this->buyOrder;
    }
}
