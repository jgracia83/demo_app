<?php

namespace Fhios\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Product
 *
 * @ORM\Table(name="E_product")
 * @ORM\Entity(repositoryClass="Fhios\EcommerceBundle\Entity\ProductRepository")
 * @Vich\Uploadable
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", scale=2)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
    * @ORM\Column(name="active", type="boolean")
    */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
    * @ORM\OneToMany(targetEntity="CartItem", mappedBy="product")
    */
    protected $cartItem;

    /**
    * @ORM\OneToMany(targetEntity="ProductReview", mappedBy="product")
    */
    protected $reviews;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="productFile", fileNameProperty="product")
     * 
     * @var File $productFile
     */
    protected $productFile;

    /**
     * @ORM\Column(type="string", length=255, name="product")
     *
     * @var string $product
     */
    protected $product;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime $updatedAt
     */
    protected $updatedAt;

    public function __tostring()
    {
        return $this->getName();
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setProductFile(File $image = null)
    {
        $this->productFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getProductFile()
    {
        return $this->productFile;
    }

    /**
     * @param string $imageName
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartItem = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reviews = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Product
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set category
     *
     * @param \Fhios\EcommerceBundle\Entity\Category $category
     * @return Product
     */
    public function setCategory(\Fhios\EcommerceBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Fhios\EcommerceBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add cartItem
     *
     * @param \Fhios\EcommerceBundle\Entity\CartItem $cartItem
     * @return Product
     */
    public function addCartItem(\Fhios\EcommerceBundle\Entity\CartItem $cartItem)
    {
        $this->cartItem[] = $cartItem;

        return $this;
    }

    /**
     * Remove cartItem
     *
     * @param \Fhios\EcommerceBundle\Entity\CartItem $cartItem
     */
    public function removeCartItem(\Fhios\EcommerceBundle\Entity\CartItem $cartItem)
    {
        $this->cartItem->removeElement($cartItem);
    }

    /**
     * Get cartItem
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCartItem()
    {
        return $this->cartItem;
    }

    /**
     * Add reviews
     *
     * @param \Fhios\EcommerceBundle\Entity\ProductReview $reviews
     * @return Product
     */
    public function addReview(\Fhios\EcommerceBundle\Entity\ProductReview $reviews)
    {
        $this->reviews[] = $reviews;

        return $this;
    }

    /**
     * Remove reviews
     *
     * @param \Fhios\EcommerceBundle\Entity\ProductReview $reviews
     */
    public function removeReview(\Fhios\EcommerceBundle\Entity\ProductReview $reviews)
    {
        $this->reviews->removeElement($reviews);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReviews()
    {
        return $this->reviews;
    }
}
