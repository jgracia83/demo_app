<?php

namespace Fhios\EcommerceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BuyOrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('created')
            ->add('customer')
            ->add('customerId')
            ->add('customerEmail')
            ->add('shippingAddress')
            ->add('billingAddress')
            ->add('totalAmount')
            ->add('status')
            ->add('cart')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fhios\EcommerceBundle\Entity\BuyOrder'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'fhios_ecommercebundle_buyorder';
    }
}
