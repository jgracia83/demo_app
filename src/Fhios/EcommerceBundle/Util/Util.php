<?php

namespace Fhios\EcommerceBundle\Util;

class Util
{
	private $container;

	public function __construct($container)
	{
		$this->container = $container;
	}

	public function totalCart($cartId)
	{
		$em = $this->container->get('doctrine')->getManager();
		$cart = $em->getRepository('EcommerceBundle:Cart')->findOneById($cartId);
		$cartItems = $cart->getCartItem();

		$total = 0;
		$subtotal = 0;
		foreach ($cartItems as $cartItem) {
			$quantity	= $cartItem->getQuantity();			
			$price 		= $cartItem->getProduct()->getPrice();
			
			$subtotal 	= $quantity * $price;
			$total 		= $subtotal + $total;
		}
		
		return $total;
	}
}
